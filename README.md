Role Name
=========

Adapting from mariadb-ansible-galera-cluster (https://github.com/adfinis-sygroup/mariadb-ansible-galera-cluster) to setup & config gelara mariadb cluster in a redhat linux envirnment

Requirements
------------

Tested with Ansible 2.7, should work with Ansible >=2.0

Role Variables
--------------
galera_cluster_group needs to be the same as the host group name

 galera_cluster_group: galera_cluster

Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

see tests/test.yml

License
-------

GPL

Author Information
------------------
bing https://gitlab.com/bingch
